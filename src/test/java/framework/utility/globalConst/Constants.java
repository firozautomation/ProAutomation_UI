package framework.utility.globalConst;

/**
 * Created by rahul.rana on 5/8/2017.
 */
public class Constants {

    public static final int WAIT_TIME = 1000;

    public static String LANGUAGE1 = "1";
    public static String LANGUAGE2 = "2";

    /**
     * C A T E G O R I E S
     */
    public static String USR_M = "male";
    public static String USR_F = "female";


}
