package framework.pageObjects.cascadeSelectBox;

import com.aventstack.extentreports.ExtentTest;
import framework.utility.common.DriverFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Created by rahulrana on 16/12/17.
 */
public class CascadeSelectBoxs_pg1 {
    private static ExtentTest pageInfo;
    /*
   page Objects
    */
    @FindBy(name = "comics")
    private WebElement selectComics;

    @FindBy(id = "hero")
    private WebElement selectHero;

    public CascadeSelectBoxs_pg1 selectComics(String text) {
        Select sel =  new Select(selectComics);
        sel.selectByVisibleText(text);
        pageInfo.info("Select Comics: "+ text);
        return this;
    }

    public CascadeSelectBoxs_pg1 selectCharacter(String text) {
        Select sel =  new Select(selectHero);
        sel.selectByVisibleText(text);
        pageInfo.info("Select Character: "+ text);
        return this;
    }

    public static CascadeSelectBoxs_pg1 init(ExtentTest t1) {
        pageInfo = t1;
        return PageFactory.initElements(DriverFactory.getDriver(), CascadeSelectBoxs_pg1.class);
    }

    public void waitForOptionsToLoad(WebElement elem){
        FluentWait<WebDriver> wait =  new FluentWait<WebDriver>(DriverFactory.getDriver());
        wait.withTimeout(10, TimeUnit.SECONDS);
        wait.pollingEvery(200, TimeUnit.MILLISECONDS);


    }
}
