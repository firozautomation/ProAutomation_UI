package framework.pageObjects.objectIdentification;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import scripts.demoScripts.ObjectIdentification;

/**
 * Created by rahulrana on 26/01/18.
 */
public class ObjectIdentificationPage extends PageInit{
    /*
    Page Objects, incorporate Page Object Model
    For creating an Object Repository
    Demonstrate use of various Selenium WebDriver Locators
     */

    /*
    Identify Using By.id
     */
    @FindBy (id = "full_name")
    WebElement txtFullName;

    /*
    Identify Using By.name
     */
    @FindBy (name = "rg-male")
    WebElement radioMale;

    /*
    Identify Using By.linkText
     */
    @FindBy (linkText = "Log-in")
    WebElement linkLoginIn;

    /*
    Identify Using By.partialLinkText
     */

    /*
    Identify Using By.className
     */
    @FindBy(className = "success_message")
    WebElement successMessage;

    /*
    Identify Using By.cssSelector
     */
    @FindBy(css = ".btn-primary")
    WebElement btnSubmit;


    /*
    Tagname task
     */
    @FindBy(id="test_tagname_table")
    WebElement webTable;

    public WebElement getTable(){
        return webTable;
    }


    public ObjectIdentificationPage(ExtentTest t1) {
        super(t1);
    }

    public static ObjectIdentificationPage init(ExtentTest t1){
        return new ObjectIdentificationPage(t1);
    }
}
