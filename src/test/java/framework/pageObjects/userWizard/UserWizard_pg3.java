package framework.pageObjects.userWizard;

import com.aventstack.extentreports.ExtentTest;
import framework.utility.common.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by rahulrana on 16/12/17.
 */
public class UserWizard_pg3 {
    private static ExtentTest pageInfo;
    /*
   Common Page objects like navigate, logout, etc
    */

    @FindBy(id = "success-message")
    private WebElement headerSuccessMessage;

    public String getSuccessmessage(){
        return headerSuccessMessage.getText();
    }

    public static UserWizard_pg3 init(ExtentTest t1){
        pageInfo = t1;
        return PageFactory.initElements(DriverFactory.getDriver(), UserWizard_pg3.class);
    }
}
