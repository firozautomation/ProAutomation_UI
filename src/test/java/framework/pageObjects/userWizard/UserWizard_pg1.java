package framework.pageObjects.userWizard;

import com.aventstack.extentreports.ExtentTest;
import framework.utility.common.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by rahulrana on 16/12/17.
 */
public class UserWizard_pg1 {
    private static ExtentTest pageInfo;
    /*
   Common Page objects like navigate, logout, etc
    */
    @FindBy(name = "firstName")
    private WebElement txtFirstName;

    @FindBy(name = "lastName")
    private WebElement txtLastName;

    @FindBy(id = "wizard-submit-next-01")
    private WebElement btnNext;

    public UserWizard_pg1 setFirstName(String text){
        txtFirstName.sendKeys(text);
        pageInfo.info("Set First Name - "+ text);
        return this;
    }

    public UserWizard_pg1 setLastName(String text){
        txtLastName.sendKeys(text);
        pageInfo.info("Set Last Name - "+ text);
        return this;
    }

    public UserWizard_pg1 clickNext(){
        btnNext.click();
        pageInfo.info("Clicked on Button Next");
        return this;
    }

    public static UserWizard_pg1  init(ExtentTest t1){
        pageInfo = t1;
        return PageFactory.initElements(DriverFactory.getDriver(), UserWizard_pg1.class);
    }
}
