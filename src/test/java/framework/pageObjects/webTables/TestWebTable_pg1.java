package framework.pageObjects.webTables;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by rahulrana on 18/11/17.
 */
public class TestWebTable_pg1 {

    /**
     * Page Objects
     */
    @FindBy (id = "static_table")
    public WebElement table1;

    @FindBy (id = "dynamic_table")
    public WebElement table2;

    @FindBy (id = "test_checkbox_table")
    public WebElement table3;

    // initialize constructor
    public TestWebTable_pg1(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

}
