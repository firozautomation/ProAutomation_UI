package framework.pageObjects.login;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.utility.common.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by rahulrana on 17/11/17.
 */
public class LinkedInLoginPage extends PageInit{


    /**
     * Page Objects
     */
    @FindBy(id = "session_key-login")
    WebElement txtEmailOrPhone;


    public LinkedInLoginPage setEmailOrPhone(String text){
        setText(txtEmailOrPhone, text, "Login Text");
        return this;
    }

    @FindBy(id = "session_password-login")
    WebElement txtPassWord;

    public LinkedInLoginPage setPassword(String text){
        setText(txtPassWord, text, "Password");
        return this;
    }

    @FindBy(name = "signin")
    WebElement btnSignIn;

    public LinkedInLoginPage clickSubmit(){
        clickOnElement(btnSignIn, "Sign In");
        return this;
    }

    public LinkedInLoginPage(ExtentTest t1) {
        super(t1);
    }

    public static LinkedInLoginPage init(ExtentTest t1){
        return new LinkedInLoginPage(t1);
    }
}
