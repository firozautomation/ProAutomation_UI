package framework.pageObjects.registration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by rahulrana on 08/11/17.
 */
public class UserRegistration_pg1{

    public static UserRegistration_pg1 init(WebDriver driver){
        return PageFactory.initElements(driver, UserRegistration_pg1.class);
    }

    /*
     * Page Objects
	 */
    @FindBy(name = "full_name")
    private WebElement txtFullName;

    public UserRegistration_pg1 setFullName(String text){
        txtFullName.sendKeys(text);
        return this;
    }

    @FindBy(name = "address")
    private WebElement txtAddress;

    @FindBy(name = "city")
    private WebElement txtCity;

    @FindBy(name = "rg-male")
    private WebElement radioMale;

    @FindBy(name = "rg-female")
    private WebElement radioFemale;

    @FindBy(name = "email")
    private WebElement txtEmail;

    @FindBy(name = "password")
    private WebElement txtPassword;

    @FindBy(name = "password_again")
    private WebElement txtPasswordAgain;

    @FindBy(name = "ch-agree")
    private WebElement chkIgree;

    @FindBy(className = "btn-primary")
    private WebElement btnSubit;


}
