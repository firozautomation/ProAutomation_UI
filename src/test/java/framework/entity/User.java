package framework.entity;

import framework.utility.common.DataFactory;

import java.util.Random;

/**
 * Created by rahul.rana on 5/5/2017.
 */
public class User {
    public String fullName, password, address, city, gender, email;

    public User(String gender) {
        this.gender = gender;
        this.fullName = "USR" + DataFactory.getRandomString(3);
        this.password = "Pas@123" + DataFactory.getRandomString(3);
        this.email = this.fullName + "@xyz.com";
        this.address = "221 Baker Street";
        this.city = "London";
    }


}
