package scripts.demoScripts;

import com.aventstack.extentreports.ExtentTest;
import framework.utility.common.DriverFactory;
import framework.utility.globalConst.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import scripts.TestInit;

/**
 * Created by rahulrana on 10/02/18.
 */
public class ElementPresentTest extends TestInit{
    @Test
    public void testElemetIsAvailable(){
        // get Driver
        WebDriver driver = DriverFactory.getDriver();
        // Test Description
        ExtentTest t1 = pNode.createNode("testElemetIsAvailable",
                "How to check if an element is present, visible or displayed on the page");

        // Open the Linked In Page
        driver.get(ConfigInput.url);


    }

}
