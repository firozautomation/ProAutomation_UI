package scripts.demoScripts;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import framework.utility.common.DriverFactory;
import framework.utility.globalConst.ConfigInput;
import framework.utility.propertiesManager.AutProperties;
import framework.utility.propertiesManager.LocaleMsgReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import scripts.TestInit;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by rahulrana on 28/11/17.
 */
public class LocalTest extends TestInit{

    /**
     * LocaleTest
     */
    @Test
    public void testLocaleMessage(){
        ExtentTest t1 = extent.createTest("Test1", "description");

        DriverFactory.getDriver().get(ConfigInput.url);
        DriverFactory.getDriver().findElement(By.className("subselect__default")).click();
        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@class='subselect__dropdownitem']/a[@data-lang='"+ConfigInput.language+"']"))).click();

        // 4386 2800 2310 4288
        // i1712070653
    }

}
