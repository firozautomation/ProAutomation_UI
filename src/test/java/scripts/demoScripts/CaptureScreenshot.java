package scripts.demoScripts;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by rahulrana on 17/11/17.
 */
public class CaptureScreenshot {
    private WebDriver driver;
    private ExtentReports extent;

    /**
     * Get the Driver
     */
    @BeforeClass
    public void beforeClassMethod() {
        // initialize driver
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        // initialize reports
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("reports/extent.html");

        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
    }

    @Test
    public void run() throws IOException {
        ExtentTest t1 = extent.createTest("Test 01", "Take ScreenShot on failure!");

        t1.pass("Successfully created Extent Reports");

        driver.get("http://www.google.com");

        // attach screenshot to report
        t1.fail("Optional Message", MediaEntityBuilder.createScreenCaptureFromPath(captureScreen()).build());
    }

    /**
     * Capture ScreenShot
     * @return
     * @throws IOException
     */
    public String captureScreen() throws IOException {
        // Get a unique name
        String fileName = System.currentTimeMillis() + ".png";

        // take the screenshot
        File imgFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        // save the screenshot to ---
        FileUtils.copyFile(imgFile, new File("./reports/ScreenShots/" + fileName));

        // returns the capture path
        return "./ScreenShots/" + fileName;
    }

    @AfterMethod
    public void afterRun(){
        extent.flush();
    }

    @AfterClass
    public void afterClass() {
        driver.quit();
    }


}
