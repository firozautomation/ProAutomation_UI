package scripts.demoScripts;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import framework.utility.common.DriverFactory;
import framework.utility.reportManager.ScreenShot;
import org.testng.annotations.Test;
import scripts.TestInit;

import java.io.IOException;

/**
 * Created by rahulrana on 12/11/17.
 */
public class DemoExtentReports extends TestInit {
    @Test
    public void runForExtent() throws IOException {
        ExtentTest t1 = pNode.createNode("Test1", "Demo for Extent Reports")
                .assignCategory("Demo").assignAuthor("rahul.rana");

        t1.info("navigate to Google and take a cool screenshot");
        //DriverFactory.getDriver().get("http://www.google.com");
        //t1.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

    }
}
