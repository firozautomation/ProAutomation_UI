package scripts.demoScripts;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageForLinkedInRegistration_pg1;
import framework.utility.Assertions;
import framework.utility.common.App;
import framework.utility.common.DriverFactory;
import org.testng.annotations.Test;
import scripts.TestInit;

/**
 * Created by rahulrana on 13/02/18.
 */
public class CaptureScreenOnFailure extends TestInit{

    @Test
    public void captureScreenOnFailure() throws Exception {
        // create extent test
        ExtentTest t1 = pNode.createNode("test-01",
                "Capture Screen On Failure And Attach to Extent Reports");

        // open the application
        App.init(t1)
                .openApplication();

        String pageTitle = DriverFactory.getDriver().getTitle();

        Assertions.verifyEqual(pageTitle, "Linked Wrong Page", "Verify Page Title", t1);

        PageForLinkedInRegistration_pg1.init(t1)
                .setFirstName("FirstName");

        This video is on Capturing Screenshot on failures and if there is a object not found exception has occurred.

                The captured screen is attached to Extent Report and also how this complete mechanism is fitting in the Pro Automation framework is explained.

                Capture Screenshot on failure and exceptions | Extent Reports | Selenium Framework | Feb 2018


    }
}
